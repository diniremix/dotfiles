#!/bin/bash

clear;

app=setupgit.sh;
uname="Jorge Brunal Pérez"
email="jorge.brunal@gmail.com"

echo "Fijando nombre de usuario y email..."
echo

git config --global user.name "$uname"
git config --global user.email $email

echo "Creando algunos alias..."
echo
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.mm "commit -m"
git config --global alias.st "status"
git config --global alias.pushm "push -u origin master"
git config --global alias.pullm "pull origin master --tags"
git config --global alias.pushd "push -u origin develop"
git config --global alias.pulld "pull origin develop --tags"
git config --global alias.logp 'log --pretty=format:"%h - %s" --graph --oneline --decorate'
git config --global alias.chp 'cherry-pick'
git config --global alias.alias "config --global --get-regexp alias"
git config --global alias.user "config --global --get-regexp .*"
git config --global alias.rmt "remote -v"
git config --global alias.fs "flow feature start"
git config --global alias.ff "flow feature finish"
git config --global alias.counter "shortlog -s -n"
git config --global merge.tool meld
git config --global core.fileMode false
git config --global core.editor nano

# Fijando cache (opcional)
git config --global credential.helper cache
echo
echo "Configuracion actual del usuario" "$uname"
echo
git user
echo
echo "Configuracion de git finalizada."
