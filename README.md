# DOTFILES

> various dotfiles and config

## Getting started

1. Change permissions of `install`

    ```sh
    chmod u+x install
    ```
1. Run `install` to copy the config files into their locations

    ```sh
    sudo ./install
    ```

## Thanks to

- [sayanee](https://github.com/sayanee/dotfiles)
