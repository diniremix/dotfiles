#!/bin/bash
#
# fedora-extras for Fedora 36+
# 2022-10-29

APP_VERSION=2022-12-18
JUST_VERSION=1.9.0
INSOMNIA_VERSION=2022.7.0
STANDARD_NOTES_VERSION=3.127.0
GOOGLECLOUDSDK_VERSION=407.0.0
USERNAME=$USER
PASSWD=

run_as_root (){
	PASSWD=`zenity --password --title "Fedora extras($1) necesita acceso de administrador"`
}

notify_service (){
	sleep 1
	notify-send -u normal "$1"
}

bash_alias() {
    # some alias
    echo 'fijando alias a bashrc'

    echo "" >> ~/.bashrc
    echo "# some alias" >> ~/.bashrc
    echo "alias la='ls -la'" >> ~/.bashrc
    echo "alias cls='clear'" >> ~/.bashrc
    echo "alias verip='curl icanhazip.com'" >> ~/.bashrc
    echo "alias matar='killall -9'" >> ~/.bashrc
    echo "alias clona='git clone'" >> ~/.bashrc
    echo "alias new-env='virtualenv --python python3.9 env'" >> ~/.bashrc
    echo "alias act-env='source env/bin/activate'" >> ~/.bashrc
    echo "alias pyserver='python3.9 -m http.server 5000'" >> ~/.bashrc
    echo "alias npm-local-list='npm list --depth=0'" >> ~/.bashrc
    echo "alias npm-global-list='npm -g list --depth=0'" >> ~/.bashrc
    echo "alias android='scrcpy -m 1024'" >> ~/.bashrc
    echo "alias ds='dnf search'" >> ~/.bashrc
    echo "alias dl='dnf list'" >> ~/.bashrc
    echo "alias di='dnf info'" >> ~/.bashrc
    echo "alias df='df -h'" >> ~/.bashrc
    echo "alias free='free -m'" >> ~/.bashrc
    # echo "alias md='mkdir -pv'" >> ~/.bashrc
    echo "md (){" >> ~/.bashrc
    echo "  mkdir -pv \$1 && cd \$_" >> ~/.bashrc
    echo "}" >> ~/.bashrc

    echo "alias .j='just -f ~/.${USERNAME}.justfile --working-directory .'" >> ~/.bashrc
    echo "alias servi='just -f ~/.servinf.justfile --working-directory .'" >> ~/.bashrc

    echo
    echo 'create alias(bashrc) finalizado'
    notify_service "create alias(bashrc) finalizado"
}

just_install (){
    cd ~/bin
    wget -c "https://github.com/casey/just/releases/download/${JUST_VERSION}/just-${JUST_VERSION}-x86_64-unknown-linux-musl.tar.gz" -O just-${JUST_VERSION}.tar.gz
    tar -zxvf just-${JUST_VERSION}.tar.gz just
    chmod +x -v just
    echo
    echo 'just task runner finalizado'
    notify_service "just task runner finalizado"
}

just_extras (){
    cd ~/bin/dotfiles
    mkdir -pv ~/.dotfiles.backup
    if [ -f ~/.${USERNAME}.justfile ]; then
        mv ~/.${USERNAME}.justfile ~/.dotfiles.backup/.${USERNAME}.justfile.original
    fi

    echo 'copiando archivos...'
    cp .diniremix.justfile ~/.${USERNAME}.justfile
    cp .servinf.justfile ~/.servinf.justfile
    echo
    echo 'just extras finalizado'
    notify_service "just extras finalizado"
}

insomnia_install (){
    cd ~/bin
    wget -c "https://github.com/Kong/insomnia/releases/download/core%40${INSOMNIA_VERSION}/Insomnia.Core-${INSOMNIA_VERSION}.rpm" -O insomnia-${INSOMNIA_VERSION}.rpm
    echo $PASSWD | sudo -S rpm -i insomnia-${INSOMNIA_VERSION}.rpm
    echo
    echo 'insomnia Rest Client finalizado'
    notify_service 'insomnia Rest Client finalizado'
}

standard_notes_install (){
    cd ~/bin
    wget -c "https://github.com/standardnotes/app/releases/download/%40standardnotes%2Fdesktop%40${STANDARD_NOTES_VERSION}/standard-notes-${STANDARD_NOTES_VERSION}-linux-x86_64.AppImage" -O standard_notes-${STANDARD_NOTES_VERSION}.AppImage
    chmod +x -v standard_notes-${STANDARD_NOTES_VERSION}.AppImage

    echo 'creando carpetas adicionales...'
    mkdir -pv ~/.local/share/icons
    mkdir -pv ~/.local/share/applications

    cd ~/bin/dotfiles
    echo 'copiando iconos y lanzadores...'
    cp -v ./icons-apps/standard-notes-Icon-512x512.png ~/.local/share/icons/standard-notes-Icon-512x512.png
    cp -v ./icons-apps/standard-notes.desktop ~/.local/share/applications/standard-notes.desktop

    echo 'estableciendo usuario y version...'
    sed -i "s/{{username}}/"$USERNAME"/g" ~/.local/share/applications/standard-notes.desktop
    sed -i "s/{{version}}/"$STANDARD_NOTES_VERSION"/g" ~/.local/share/applications/standard-notes.desktop

    echo
    echo 'standard notes finalizado'
    notify_service "standard notes finalizado"
}

git_alias (){
    cd ~/bin/dotfiles
    mkdir -pv ~/.dotfiles.backup
    if [ -f ~/.gitconfig ]; then
        mv ~/.gitconfig ~/.dotfiles.backup/.gitconfig.original
    fi
    bash ./setupgit.sh
    echo
    echo 'git alias finalizado'
    notify_service "git alias finalizado"
}

nano_extras (){
    # nano config files in /usr/share/nano
    cd ~/bin/dotfiles
    mkdir -pv ~/.dotfiles.backup
    if [ -f ~/.nanorc ]; then
        mv -v ~/.nanorc ~/.dotfiles.backup/.nanorc.original
    fi
    cp .nanorc ~/.nanorc
    echo
    echo 'nano extras finalizado'
    notify_service "nano extras finalizado"
}

googlecloud_sdk_install (){
    cd ~/bin
    wget -c "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-${GOOGLECLOUDSDK_VERSION}-linux-x86_64.tar.gz" -O google-cloud-sdk-${GOOGLECLOUDSDK_VERSION}.tar.gz
    tar -xzvf google-cloud-sdk-${GOOGLECLOUDSDK_VERSION}.tar.gz
    echo
    echo 'google cloud sdk finalizado'
    notify_service "google cloud sdk finalizado"
}

fedora_themes (){
    cd ~/bin/
    echo
    echo 'descargando Orchis-theme'
    git clone https://github.com/vinceliuice/Orchis-theme.git
    echo
    echo 'descargando Tela-icon-theme'
    git clone https://github.com/vinceliuice/Tela-icon-theme
    echo
    echo 'fedora themes finalizado'
    notify_service "fedora themes finalizado"
}

update_repo (){
    cd ~/bin/dotfiles
    echo
    echo 'actualizando el repo dotfiles...'
    git pull origin master
    echo
    notify_service "update repo finalizado"
}

script_version() {
    echo "Fedora extras v.${APP_VERSION}"
    $(zenity --info --text="Fedora extras v.${APP_VERSION}\nScript para instalar algunas aplicaciones\nque no se encuentran en los repos oficiales o RPM fusión\n\nAdemás de otras configuraciones\n\nmás info en: https://gitlab.com/diniremix/dotfiles")
    echo
    echo 'script version finalizado'
}

# Exit with confirmation
bye_bye() {
    echo 'saliendo...'
    if ! [ -x "$(command -v gcloud 2>/dev/null )" ]; then
        echo 'google-cloud-sdk:'
        echo 'ejecute ~/bin/google-cloud-sdk/install.sh'
        echo 'para continuar la instalacion del sdk de gcloud'
    fi
    exit 12
}

# zenity main gui
main_gui (){
	resp=$(zenity --width=550 --height=550 --list --text "Fedora extras v.${APP_VERSION}" --radiolist --column "#" --column "Acciones" --column "Notas" TRUE "bash alias" " " FALSE "just" " " FALSE "Insomnia rpm" "necesita root" FALSE "standard notes" " " FALSE "git alias" " " FALSE "nano extras" " " FALSE "google cloud sdk" " " FALSE "temas e iconos" "Orchis-theme & Tela-icon-theme" FALSE "update repo" " " FALSE "acerca de" " " FALSE "salir" " ");
}

# The main function
main() {
    if ! [ -x "$(command -v wget 2>/dev/null )" ]; then
        echo "Fedora extras v.${APP_VERSION} Error: wget is not installed."
        exit
    fi
    if ! [ -x "$(command -v zenity 2>/dev/null )" ]; then
        echo "Fedora extras v.${APP_VERSION} Error: zenity is not installed."
        exit
    fi
    while true; do
        main_gui
        case $resp in
			"bash alias")
				clear && bash_alias ;;
			"just")
				clear && just_install && just_extras ;;
			"Insomnia rpm")
                clear && run_as_root "Insomnia" && insomnia_install ;;
			"standard notes")
				clear && standard_notes_install ;;
			"git alias")
				clear && git_alias;;
			"nano extras")
				clear && nano_extras;;
			"google cloud sdk")
				clear && googlecloud_sdk_install ;;
			"temas e iconos")
				clear && fedora_themes ;;
			"update repo")
				clear && update_repo ;;
			"acerca de")
				clear && script_version ;;
			"salir")
				bye_bye ;;
            * ) bye_bye ;;
		esac
        PASSWD=
    done
}
# End

# Run the main function
main
