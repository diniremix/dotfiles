# Just task runner

# 2022-07-14
# 2024-09-09
set dotenv-load := false

python_env := "python3.9"
python37 := "python3.7"
pyenv37 := "3.7.15"
current_python_version := "3.9"
uv_python_version := "3.11"

# Retrieves the path of the current working directory
current_path := invocation_directory()
just_home := justfile_directory()
just_exec := just_executable()

# more info
# https://github.com/casey/just#changing-the-working-directory-in-a-recipe

# help
default: help


# configure a new repo using git flow
repo folder:
    @echo "creando repo en {{ current_path }}/{{ folder }}"
    @echo "directorio de trabajo: {{ folder }}"

    @mkdir -pv {{ folder }}

    @echo
    @echo "iniciando git flow..."

    @cd {{ folder }} && git flow init -fd

    @echo
    @echo "proceso completado en: {{ current_path }}/{{ folder }}"


# create a new env and install deps using python 3.11
py app:
    #!/usr/bin/env bash

    echo "directorio actual: {{ current_path }}"
    mkdir -pv {{app}}
    cd {{app}}

    echo "configurando... '{{ file_name(current_path) }}/{{app}}'"

    if [ -d .venv ]; then
        echo "Error: ya existe un entorno (.venv) en la carpeta '{{ app }}'"
        echo "saliendo..."
        exit
    fi

    if [ -f requirements.txt ]; then
        echo "Error: ya existe un archivo 'requirements.txt' en la carpeta '{{ app }}'"
        echo "saliendo..."
        exit
    fi

    if [ -f main.py ]; then
        echo "ya existe un archivo 'main.py', omitiendo..."
    else
        touch main.py
        echo "# -*- coding: utf-8 -*-" > main.py
    fi

    touch requirements.txt
    echo "creando entorno..."
    python3.11 -m venv .venv

    echo
    echo "proceso completado en: {{ current_path }}/{{ app }}"


# create a new env and install deps using old python version
py37:
    #!/usr/bin/env bash

    echo "considera migrar tus proyectos a Python 3.9 o superior..."
    echo "validando..."

    if [ -d env ]; then
        echo "Error: ya existe un entorno (env) en esta ruta, saliendo..."
        exit
    fi

    if [ -f requirements.txt ]; then
        echo "creando entorno con {{ python37 }} ..."
        echo
    else
        echo "Error: {{ file_name(current_path) }} no contiene un archivo requirements.txt"
        exit
    fi

    env_method="virtualenv"

    if ! [ -x "$(command -v virtualenv 2>/dev/null )" ]; then
        echo "Error: virtualenv is not installed."
        echo "more info: https://virtualenv.pypa.io/en/latest/installation.html"
        echo
        echo "intentando con pyenv..."
        echo

        if ! [ -x "$(command -v pyenv 2>/dev/null )" ]; then
            echo "Error: pyenv is not installed."
            echo "more info: https://github.com/pyenv/pyenv"
            echo
            exit
        fi

        env_method="pyenv"
    fi

    if [ $env_method = "virtualenv" ]; then
        echo "creando entorno con virtualenv ({{python37}})..."
        virtualenv --python {{ python37 }} env
    else
        echo "creando entorno con pyenv... {{pyenv37}}"
        pyenv versions
        pyenv local {{ pyenv37 }}
        # cat .python-version
        python -m venv env
    fi

    if [ $? -gt 0 ]; then
        echo "Error: no se pudo crear el entorno."
        exit
    fi

    echo
    echo "activando entorno..."

    source env/bin/activate

    echo "instalando dependencias desde requirements.txt con pip..."

    pip install -r requirements.txt

    echo
    echo "actualizando pip..."

    pip install --upgrade pip

    echo
    echo "proceso completado en: {{ current_path }}"


# create a new env and install deps using Poetry
py39:
    #!/usr/bin/env bash

    echo "validando..."

    if ! [ -x "$(command -v poetry 2>/dev/null )" ]; then
        echo "Error: poetry is not installed."
        echo "more info: https://python-poetry.org/docs/master/#installing-with-the-official-installer"
        exit
    fi

    if [ -f pyproject.toml ]; then
        echo "creando entorno con Poetry y {{python_env}}..."
        echo
    else
        echo "{{ file_name(current_path) }} no contiene un archivo pyproject.toml"
        exit
    fi

    poetry env use {{ python_env }}

    if [ $? -gt 0 ]; then
        echo
        echo "Ocurrio un error al iniciar poetry, revisa tu archivo pyproject.toml..."
        exit 1
    fi

    echo
    echo "instalando dependencias principales (--no-root)..."
    echo

    poetry install --no-root

    if [ $? -gt 0 ]; then
        echo
        echo "Ocurrio un error al instalar las dependencias."
        exit 1
    fi

    echo
    echo "proceso completado en: {{ current_path }}"


# create a new bin app or lib using Rust
rust app type="bin":
    #!/usr/bin/env bash

    echo "directorio actual: {{ current_path }}"

    if [ {{type}} = "lib" ]; then
        echo "creando libreria en la carpeta: {{app}}..."
        echo
        cargo new {{app}} --lib --verbose
    else
        echo "creando aplicación en la carpeta: {{app}}..."
        echo
        cargo new {{app}} --verbose
    fi

    echo
    echo "proceso completado en: {{ current_path }}/{{ app }}"


# hello world
test:
    @echo "Testing…"
    @echo "ejecuta:"
    @echo "> just -l"
    @echo "para más información"


# show justfile config file
help:
    @echo
    @echo "The config file is at: {{ just_home }}"
    @echo "create alias with:"
    @echo "'just -f ~/.USERNAME.justfile --working-directory .'"
    @echo "If you want some recipes to be available everywhere"
    @echo "more info: https://github.com/casey/just#user-justfiles"


home:
    @echo The executable is at {{ just_exec }}


# activate ssh accounts
ssh:
    @echo "activating ssh accounts..."
    @eval $(ssh-agent -s)
    @ssh-add ~/.ssh/gitlab
    @echo "done!"


# cleaning the house...
clean:
    @echo "cleaning the house..."
    history -c


# displays statistics about your code
loc:
    #!/usr/bin/env bash

    if ! [ -x "$(command -v tokei 2>/dev/null )" ]; then
        echo "Error: tokei is not installed."
        echo "more info: https://github.com/XAMPPRocky/tokei#installation"
        exit
    fi

    default_dir="src"

    if [ -d src ]; then
        default_dir="src"
    elif [ -d app ]; then
        default_dir="app"
    fi

    echo "directorio de trabajo: {{ file_name(current_path) }} ($default_dir)"

    tokei --hidden $default_dir \
    --exclude .env --exclude env \
    --exclude .venv --exclude venv \
    --exclude test \
    --exclude dist \
    --exclude public

# displays statistics about your code using scc
loc2:
    #!/usr/bin/env bash

    if ! [ -x "$(command -v scc 2>/dev/null )" ]; then
        echo "Error: scc is not installed."
        echo "more info: https://github.com/boyter/scc"
        exit
    fi

    default_dir="./"

    if [ -d src ]; then
        default_dir="src"
    elif [ -d app ]; then
        default_dir="app"
    fi

    echo "directorio de trabajo: {{ file_name(current_path) }} ($default_dir)"

    scc $default_dir \
    --exclude-dir .env,env,.venv,venv,test,dist,public,target \
    -u -v

# displays weather
clima:
    @curl -H 'Accept-Language: es' 'wttr.in/bogota?m2'


# displays current IP
ip:
    @curl icanhazip.com

# create a new bin app or lib using Python and uv
python app:
    #!/usr/bin/env bash

    if ! [ -x "$(command -v uv 2>/dev/null )" ]; then
        echo "Error: uv is not installed."
        echo "more info: https://docs.astral.sh/uv/"
        exit
    fi

    echo "directorio actual: {{current_path}}"
    echo "creando python packagen en la carpeta: {{app}}..."

    uv init --python {{uv_python_version}} {{current_path}}/{{app}}
    uv venv --python {{uv_python_version}} --directory {{current_path}}/{{app}}

    echo
    echo "proceso completado en: {{current_path}}/{{app}}"
